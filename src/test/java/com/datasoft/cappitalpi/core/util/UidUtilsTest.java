package com.datasoft.cappitalpi.core.util;

import com.datasoft.cappitalpi.core.domain.BaseEntity;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class UidUtilsTest {

    private static final long ID = 100L;

    @Test
    public void givenInvalidId_whenGenerateUid_thenReturnNull() {
        Long decid = UidUtils.getDecId(null);
        Truth.assertThat(decid).isNull();

        Truth.assertThat(UidUtils.getEncId(decid)).isNull();
    }

    @Test
    public void givenId_whenGenerateUid_thenReturnUid() {
        String uid = UidUtils.getEncId(ID);
        Truth.assertThat(uid).isNotNull();

        Long id = UidUtils.getDecId(uid);
        Truth.assertThat(id).isEqualTo(ID);

        BaseEntity entity = new BaseEntity();
        entity.setId(10L);
        Truth.assertThat(entity.getUid()).isNotNull();

        BaseEntity newEntity = new BaseEntity();
        Truth.assertThat(newEntity.isNew()).isTrue();
        Truth.assertThat(newEntity.getId()).isNull();
    }

}