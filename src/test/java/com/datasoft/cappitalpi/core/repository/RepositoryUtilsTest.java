package com.datasoft.cappitalpi.core.repository;

import com.datasoft.cappitalpi.core.TestEntity;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.repository.support.Repositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RepositoryUtilsTest {

    @Mock
    private TestRepository repository;
    @Mock
    private Repositories repositories;
    private RepositoryUtils repositoryUtils;

    @BeforeEach
    public void setUp() {
        repositoryUtils = new RepositoryUtils(repositories);
    }

    @Test
    public void givenNewCode_whenGenerateCode_thenReturnCode() {
        TestEntity entity = new TestEntity();
        when(repositories.getRepositoryFor(any())).thenReturn(Optional.of(repository));
        when(repository.findOne(any())).thenReturn(Optional.empty());
        repositoryUtils.generateCode(entity);
        Truth.assertThat(entity.getCode()).isNotNull();
        Truth.assertThat(entity.getCode()).isGreaterThan(0);
        Mockito.verify(repository, times(1)).findOne(any());
    }

    @Test
    public void givenExistingCode_whenGenerateCode_thenSkipCodeGeneration() {
        final Long code = 10L;
        TestEntity entity = new TestEntity();
        entity.setCode(code);
        when(repositories.getRepositoryFor(any())).thenReturn(Optional.of(repository));
        when(repository.findOne(any())).thenReturn(Optional.empty());
        repositoryUtils.generateCode(entity);
        Truth.assertThat(entity.getCode()).isNotNull();
        Truth.assertThat(entity.getCode()).isEqualTo(code);
        Mockito.verify(repository, times(0)).findOne(any());
    }

    @Test
    public void givenListOfEntities_whenGenerateCode_thenReturnListWithCode() {
        List<TestEntity> list = new ArrayList<>();
        list.add(new TestEntity());
        list.add(new TestEntity());
        list.add(new TestEntity());
        list.add(new TestEntity());
        list.forEach(testEntity -> Truth.assertThat(testEntity.getCode()).isNull());
        when(repositories.getRepositoryFor(any())).thenReturn(Optional.of(repository));
        repositoryUtils.generateCodes(list);
        list.forEach(testEntity -> Truth.assertThat(testEntity.getCode()).isNotNull());
    }

    @Test
    public void givenInvalidClass_whenGenerateCode_thenThrowException() {
        Assertions.assertThrows(Exception.class, () -> repositoryUtils.generateCode(new Object()));
        Mockito.verify(repository, times(0)).findOne(any());
    }

    public interface TestRepository extends GenericRepository<TestEntity> {

    }

}