package com.datasoft.cappitalpi.core.annotation.support;

import com.datasoft.cappitalpi.core.TestEntity;
import com.datasoft.cappitalpi.core.TestUtils;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ColumnLengthAnnotationTest {

    private ColumnLengthAnnotation columnLengthAnnotation;

    @BeforeEach
    void setUp() {
        columnLengthAnnotation = new ColumnLengthAnnotation(TestUtils.messageTranslator());
    }

    @Test
    void givenNullObject_whenValidate_thenReturnNull() {
        String validate = columnLengthAnnotation.validate(null);
        Truth.assertThat(validate).isNull();
    }

    @Test
    void givenValidTestEntityObject_whenValidate_thenReturnNull() {
        TestEntity entity = new TestEntity();
        entity.setName(RandomStringUtils.randomAlphabetic(20));
        String validate = columnLengthAnnotation.validate(entity);
        Truth.assertThat(validate).isNull();
    }

    @Test
    void givenInvalidTestEntityObject_whenValidate_thenReturnNull() {
        TestEntity entity = new TestEntity();
        entity.setName(RandomStringUtils.randomAlphabetic(50));
        String result = columnLengthAnnotation.validate(entity);
        Truth.assertThat(result).isNotNull();
        Truth.assertThat(result).isNotEmpty();
    }

    @Test
    void returnType() {
        AnnotationSupportType type = columnLengthAnnotation.type();
        Truth.assertThat(type).isEqualTo(AnnotationSupportType.LENGTH);
    }
}