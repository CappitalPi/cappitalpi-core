package com.datasoft.cappitalpi.core.annotation.support;

import com.datasoft.cappitalpi.core.TestEntity;
import com.datasoft.cappitalpi.core.TestUtils;
import com.datasoft.cappitalpi.core.config.resource.CoreMessageIdResource;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class JoinColumnNullableAnnotationTest {

    private JoinColumnNullableAnnotation joinColumnNullableAnnotation;

    @BeforeEach
    void setUp() {
        joinColumnNullableAnnotation = new JoinColumnNullableAnnotation(TestUtils.messageTranslator());
    }

    @Test
    void givenNullObject_whenValidate_thenReturnNull() {
        String validate = joinColumnNullableAnnotation.validate(null);
        Truth.assertThat(validate).isNull();
    }

    @Test
    void givenValidTestEntityObject_whenValidate_thenReturnNull() {
        TestEntity entity = new TestEntity();
        entity.setCode(9L);
        entity.setName(RandomStringUtils.randomAlphabetic(20));
        entity.setJoinColumn(RandomStringUtils.randomAlphabetic(12));
        entity.setJoinColumnInt(10L);
        String validate = joinColumnNullableAnnotation.validate(entity);
        Truth.assertThat(validate).isNull();
    }

    @Test
    void givenInvalidTestEntityObject_whenValidate_thenThrowException() {
        TestEntity entity = new TestEntity();
        String validate = joinColumnNullableAnnotation.validate(entity);
        Truth.assertThat(validate).isNotNull();
        Truth.assertThat(validate).contains(TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "joinColumn"));
        Truth.assertThat(validate).contains(TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "joinColumnInt"));
    }

    @Test
    void returnType() {
        AnnotationSupportType type = joinColumnNullableAnnotation.type();
        Truth.assertThat(type).isEqualTo(AnnotationSupportType.JOIN_COLUMN_NULLABLE);
    }

}