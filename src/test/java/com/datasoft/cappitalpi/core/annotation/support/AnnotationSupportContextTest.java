package com.datasoft.cappitalpi.core.annotation.support;

import com.datasoft.cappitalpi.core.TestEntity;
import com.datasoft.cappitalpi.core.TestUtils;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.AppException;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;

@ExtendWith(SpringExtension.class)
class AnnotationSupportContextTest {

    private AnnotationSupportContext context;

    @BeforeEach
    public void setUp() {
        MessageTranslator messageTranslator = TestUtils.messageTranslator();
        ColumnLengthAnnotation columnLengthAnnotation = new ColumnLengthAnnotation(messageTranslator);
        ColumnNullableAnnotation columnNullableAnnotation = new ColumnNullableAnnotation(messageTranslator);
        Set<AnnotationSupport> strategies = new HashSet<>();
        strategies.add(columnLengthAnnotation);
        strategies.add(columnNullableAnnotation);
        context = new AnnotationSupportContext(strategies);
    }

    @Test
    void givenInvalidClass_whenGetContext_thenThrowException() {
        Assertions.assertThrows(AppException.class, () -> context.validate(null, new TestEntity()));
    }

    @Test
    void givenInvalidEntity_whenValidateLength_thenColumnInfo() {
        TestEntity testEntity = new TestEntity();
        testEntity.setName(RandomStringUtils.randomAlphabetic(50));
        String result = context.validate(AnnotationSupportType.LENGTH, testEntity);
        Truth.assertThat(result).isNotNull();
        Truth.assertThat(result).isNotEmpty();
    }

    @Test
    void givenValidEntity_whenValidateLength_thenReturnNull() {
        TestEntity testEntity = new TestEntity();
        testEntity.setName(RandomStringUtils.randomAlphabetic(30));
        String result = context.validate(AnnotationSupportType.LENGTH, testEntity);
        Truth.assertThat(result).isNull();
    }
}