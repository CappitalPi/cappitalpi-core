package com.datasoft.cappitalpi.core.annotation.support;

import com.datasoft.cappitalpi.core.ChildOfTestEntity;
import com.datasoft.cappitalpi.core.TestEntity;
import com.datasoft.cappitalpi.core.TestUtils;
import com.datasoft.cappitalpi.core.config.resource.CoreMessageIdResource;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ColumnNullableAnnotationTest {

    private ColumnNullableAnnotation columnNullableAnnotation;

    @BeforeEach
    void setUp() {
        columnNullableAnnotation = new ColumnNullableAnnotation(TestUtils.messageTranslator());
    }

    @Test
    void givenNullObject_whenValidate_thenReturnNull() {
        String validate = columnNullableAnnotation.validate(null);
        Truth.assertThat(validate).isNull();
    }

    @Test
    void givenValidTestEntityObject_whenValidate_thenReturnNull() {
        TestEntity entity = new TestEntity();
        entity.setCode(9L);
        entity.setName(RandomStringUtils.randomAlphabetic(20));
        String validate = columnNullableAnnotation.validate(entity);
        Truth.assertThat(validate).isNull();
    }

    @Test
    void givenInvalidTestEntityObject_whenValidate_thenReturnMessage() {
        TestEntity entity = new TestEntity();
        entity.setName(null);
        String result = columnNullableAnnotation.validate(entity);
        Truth.assertThat(result).isNotNull();
        Truth.assertThat(result).isNotEmpty();
        Truth.assertThat(result).contains(TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "name"));
        Truth.assertThat(result).contains(TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "code"));
    }

    @Test
    void givenInvalidChildTestEntityField_whenValidate_thenReturnMessage() {
        ChildOfTestEntity entity = new ChildOfTestEntity();
        entity.setCode(9L);
        entity.setName(RandomStringUtils.randomAlphabetic(20));
        String result = columnNullableAnnotation.validate(entity);
        Truth.assertThat(result).isNotNull();
        Truth.assertThat(result).isNotEmpty();
        Truth.assertThat(result).isEqualTo(TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "description"));
    }

    @Test
    void givenParentOfChildTestEntityField_whenValidate_thenReturnMessage() {
        ChildOfTestEntity entity = new ChildOfTestEntity();
        entity.setCode(9L);
        entity.setDescription(RandomStringUtils.randomAlphabetic(20));
        String result = columnNullableAnnotation.validate(entity);
        Truth.assertThat(result).isNotNull();
        Truth.assertThat(result).isNotEmpty();
        Truth.assertThat(result).isEqualTo(TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "name"));
    }

    @Test
    void givenParentOfParentOfChildTestEntityField_whenValidate_thenReturnMessage() {
        ChildOfTestEntity entity = new ChildOfTestEntity();
        entity.setName(RandomStringUtils.randomAlphabetic(20));
        entity.setDescription(RandomStringUtils.randomAlphabetic(20));
        String result = columnNullableAnnotation.validate(entity);
        Truth.assertThat(result).isNotNull();
        Truth.assertThat(result).isNotEmpty();
        Truth.assertThat(result).isEqualTo(TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "code"));
    }

    @Test
    void givenInvalidParentTestEntityObject_whenValidate_thenReturnMessage() {
        TestEntity entity = new TestEntity();
        entity.setName(RandomStringUtils.randomAlphabetic(20));
        String result = columnNullableAnnotation.validate(entity);
        Truth.assertThat(result).isNotNull();
        Truth.assertThat(result).isNotEmpty();
        Truth.assertThat(result).isEqualTo(TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "code"));
    }

    @Test
    void returnType() {
        AnnotationSupportType type = columnNullableAnnotation.type();
        Truth.assertThat(type).isEqualTo(AnnotationSupportType.COLUMN_NULLABLE);
    }
}