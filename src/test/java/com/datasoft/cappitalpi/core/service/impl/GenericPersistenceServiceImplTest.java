package com.datasoft.cappitalpi.core.service.impl;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.datasoft.cappitalpi.core.TestEntity;
import com.datasoft.cappitalpi.core.observer.BaseObserver;
import com.datasoft.cappitalpi.core.repository.GenericRepository;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class GenericPersistenceServiceImplTest {

    private static final String OBSERVER_EXECUTED = "Observer has been executed successfully!";
    private final Logger logger = (Logger) LoggerFactory.getLogger(TestObserver.class);
    private final ListAppender<ILoggingEvent> listAppender = new ListAppender<>();

    @Mock
    private GenericRepository<TestEntity> repository;
    @Mock
    private GenericRepository<Object> invalidRepository;
    private TestService testService;
    private TestObserver testObserver;

    @BeforeEach
    public void setUp() {
        listAppender.start();
        logger.addAppender(listAppender);

        testObserver = new TestObserver();
        testService = new TestService(repository);
    }

    @Test
    public void givenInvalidObject_whenPersist_thenThrowException() {
        when(repository.save(any())).thenThrow(RuntimeException.class);
        Assertions.assertThrows(RuntimeException.class, () ->
                testService.persist(new TestEntity())
        );
        Mockito.verify(repository, times(1)).save(any());
    }

    @Test
    public void givenObject_whenPersist_thenObjectIsPersisted() {
        testService.persist(new TestEntity());
        Mockito.verify(repository, times(1)).save(any());
        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNull(event);
    }

    @Test
    public void givenObject_whenPersistWithObservable_thenObjectIsPersisted() {
        testService.persist(new TestEntity(), testObserver);
        Mockito.verify(repository, times(1)).save(any());

        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNotNull(event);
        assertNotNull(event.getMessage());
        assertTrue(event.getMessage().contains(OBSERVER_EXECUTED));
    }

    @Test
    public void givenInvalidObject_whenUpdate_thenThrowException() {
        when(repository.save(any())).thenThrow(RuntimeException.class);
        Assertions.assertThrows(RuntimeException.class, () -> testService.update(new TestEntity(), 1L));
        Mockito.verify(repository, times(1)).save(any());
        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNull(event);
    }

    @Test
    public void givenInvalidId_whenUpdate_thenReturnNull() {
        TestEntity update = testService.update(new TestEntity(), null);
        Truth.assertThat(update).isNull();
        Mockito.verify(repository, times(0)).save(any());
        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNull(event);
    }

    @Test
    public void givenObject_whenUpdate_thenObjectIsUpdated() {
        testService.update(new TestEntity(), 1L);
        Mockito.verify(repository, times(1)).save(any());
        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNull(event);
    }

    @Test
    public void givenObject_whenUpdateWithObservable_thenObjectIsUpdated() {
        testService.update(new TestEntity(), 1L, testObserver);
        Mockito.verify(repository, times(1)).save(any());

        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNotNull(event);
        assertNotNull(event.getMessage());
        assertTrue(event.getMessage().contains(OBSERVER_EXECUTED));
    }

    @Test
    public void givenInvalidObject_whenDelete_thenReturnNull() {
        when(repository.findById(any())).thenReturn(Optional.empty());
        TestEntity o = testService.remove(10L);
        Truth.assertThat(o).isNull();
        Mockito.verify(repository, times(1)).findById(any());
        Mockito.verify(repository, times(0)).delete(any());
        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNull(event);
    }

    @Test
    public void givenInvalidObject_whenDelete_thenThrowException() {
        when(repository.findById(any())).thenReturn(Optional.of(new TestEntity()));
        doThrow(RuntimeException.class).when(repository).delete(any());
        Assertions.assertThrows(RuntimeException.class, () -> testService.remove(1L));
        Mockito.verify(repository, times(1)).findById(any());
        Mockito.verify(repository, times(1)).delete(any());
        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNull(event);
    }

    @Test
    public void givenObject_whenDelete_thenObjectIsDeleted() {
        when(repository.findById(any())).thenReturn(Optional.of(new TestEntity()));
        TestEntity o = testService.remove(1L);
        Mockito.verify(repository, times(1)).delete(any());
        Truth.assertThat(o).isNotNull();
        Mockito.verify(repository, times(1)).findById(any());
        Mockito.verify(repository, times(1)).delete(any());
        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNull(event);
    }

    @Test
    public void givenObject_whenDeleteWithObservable_thenObjectIsDeleted() {
        when(repository.findById(any())).thenReturn(Optional.of(new TestEntity()));

        testService.remove(10L, testObserver);
        Mockito.verify(repository, times(1)).delete(any());

        ILoggingEvent event = listAppender.list.stream().filter(iLoggingEvent -> Level.INFO.equals(iLoggingEvent.getLevel())).findFirst().orElse(null);
        assertNotNull(event);
        assertNotNull(event.getMessage());
        assertTrue(event.getMessage().contains(OBSERVER_EXECUTED));
    }

    @Test
    public void whenFindAll_thenReturnAll() {
        ArrayList<TestEntity> objects = new ArrayList<>();
        objects.add(new TestEntity());
        when(repository.findAll()).thenReturn(objects);
        List<TestEntity> all = testService.findAll();
        Truth.assertThat(all).isNotEmpty();
        Mockito.verify(repository, times(1)).findAll();
    }

    private static class TestService extends GenericPersistenceServiceImpl<TestEntity> {

        private final GenericRepository<TestEntity> repository;

        private TestService(GenericRepository<TestEntity> repository) {
            this.repository = repository;
        }

        @Override
        protected GenericRepository<TestEntity> getRepository() {
            return repository;
        }
    }

    private static class InvalidService extends GenericPersistenceServiceImpl<Object> {

        private final GenericRepository<Object> repository;

        private InvalidService(GenericRepository<Object> repository) {
            this.repository = repository;
        }

        @Override
        protected GenericRepository<Object> getRepository() {
            return repository;
        }
    }

    private static class TestObserver implements BaseObserver {

        private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(TestObserver.class);

        @Transactional(propagation = Propagation.REQUIRED)
        @Override
        public void notifyObserver(Object arg) {
            LOGGER.info(OBSERVER_EXECUTED);
        }
    }

}