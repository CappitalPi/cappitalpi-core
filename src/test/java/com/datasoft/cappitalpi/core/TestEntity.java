package com.datasoft.cappitalpi.core;

import com.datasoft.cappitalpi.core.domain.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;

public class TestEntity extends BaseEntity {

    @Column(length = 30, nullable = false)
    private String name;
    @Column
    private String phoneNumber;
    @JoinColumn(nullable = false)
    private String joinColumn;
    @JoinColumn(nullable = false)
    private Long joinColumnInt;


    public TestEntity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getJoinColumn() {
        return joinColumn;
    }

    public void setJoinColumn(String joinColumn) {
        this.joinColumn = joinColumn;
    }

    public Long getJoinColumnInt() {
        return joinColumnInt;
    }

    public void setJoinColumnInt(Long joinColumnInt) {
        this.joinColumnInt = joinColumnInt;
    }
}