package com.datasoft.cappitalpi.core.command;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class InvokeCommandTest {

    @Test
    public void givenActions_whenInvokeCommand_thenCommandIsInvoked() {
        List<String> values = new ArrayList<>();
        InvokeCommand command = new InvokeCommand.Builder()
                .add(() -> values.add("value 1"))
                .add(() -> values.add("value 2"))
                .add(() -> values.add("value 3"))
                .build();
        Truth.assertThat(values).isEmpty();
        command.execute();
        Truth.assertThat(values).isNotEmpty();
    }

}