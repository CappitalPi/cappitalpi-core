package com.datasoft.cappitalpi.core.validator;

import com.datasoft.cappitalpi.core.TestEntity;
import com.datasoft.cappitalpi.core.TestUtils;
import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupport;
import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportContext;
import com.datasoft.cappitalpi.core.annotation.support.ColumnLengthAnnotation;
import com.datasoft.cappitalpi.core.annotation.support.ColumnNullableAnnotation;
import com.datasoft.cappitalpi.core.annotation.support.JoinColumnNullableAnnotation;
import com.datasoft.cappitalpi.core.config.resource.CoreMessageIdResource;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class AbstractValidationBeanTest {

    private static final String ERROR_MESSAGE_1 = "Error Message 1";
    private static final String ERROR_MESSAGE_2 = "Error Message 2";

    private TestValidator testValidator;

    @BeforeEach
    void setUp() {
        MessageTranslator messageTranslator = TestUtils.messageTranslator();
        Set<AnnotationSupport> strategies = new HashSet<>();
        strategies.add(new ColumnLengthAnnotation(messageTranslator));
        strategies.add(new ColumnNullableAnnotation(messageTranslator));
        strategies.add(new JoinColumnNullableAnnotation(messageTranslator));

        testValidator = new TestValidator(messageTranslator);
        testValidator.setContext(new AnnotationSupportContext(strategies));
    }

    @Test
    void clearErrorMessages() {
        Truth.assertThat(testValidator.getErrorMessages()).isEmpty();
        Truth.assertThat(testValidator.hasErrors()).isFalse();
        testValidator.addMessage(ERROR_MESSAGE_1);
        testValidator.addMessage(ERROR_MESSAGE_2);
        Truth.assertThat(testValidator.getErrorMessages()).isNotEmpty();
        Truth.assertThat(testValidator.hasErrors()).isTrue();
        testValidator.clearErrorMessages();
        Truth.assertThat(testValidator.getErrorMessages()).isEmpty();
        Truth.assertThat(testValidator.hasErrors()).isFalse();
    }

    @Test
    void givenNullValue_whenValidate_thenThrowException() {
        Assertions.assertThrows(ValidationException.class, () -> testValidator.validate(null));
    }

    @Test
    void givenValidValue_whenValidate_thenDoNotThrowException() throws ValidationException {
        TestEntity entity = new TestEntity();
        entity.setId(Long.parseLong(RandomStringUtils.randomNumeric(9)));
        entity.setCode(Long.parseLong(RandomStringUtils.randomNumeric(9)));
        entity.setName(RandomStringUtils.randomAlphabetic(30));
        entity.setJoinColumn(RandomStringUtils.randomAlphabetic(30));
        entity.setJoinColumnInt(30L);
        testValidator.validate(entity);
    }

    @Test
    void givenInvalidValue_whenValidateAndSkipValidation_thenDoNotThrowException() throws ValidationException {
        TestEntity entity = new TestEntity();
        entity.setId(Long.parseLong(RandomStringUtils.randomNumeric(9)));
        entity.setCode(Long.parseLong(RandomStringUtils.randomNumeric(9)));
        entity.setName(null);
        testValidator.skipValidations();
        testValidator.validate(entity);
    }

    @Test
    void givenInvalidLength_whenValidateAndSkipValidation_thenDoNotThrowException() throws ValidationException {
        TestEntity entity = new TestEntity();
        entity.setId(Long.parseLong(RandomStringUtils.randomNumeric(9)));
        entity.setCode(Long.parseLong(RandomStringUtils.randomNumeric(9)));
        entity.setName(RandomStringUtils.randomAlphabetic(50));
        testValidator.skipValidations();
        testValidator.validate(entity);
    }

    @Test
    void givenInvalidLength_whenValidate_thenThrowException() {
        TestEntity entity = new TestEntity();
        entity.setId(Long.parseLong(RandomStringUtils.randomNumeric(9)));
        entity.setCode(Long.parseLong(RandomStringUtils.randomNumeric(9)));
        entity.setName(RandomStringUtils.randomAlphabetic(50));
        Assertions.assertThrows(ValidationException.class, () -> testValidator.validate(entity));
    }

    @Test
    void formatMessage() {
        String param1 = "parameter 1";
        String param2 = "parameter 2";
        String result = testValidator.formatMessage(CoreMessageIdResource.Validation.SERVER_ERROR, param1, param2);
        Truth.assertThat(result).isEqualTo(TestUtils.formatMessage(CoreMessageIdResource.Validation.SERVER_ERROR));
    }

    static class TestValidator extends AbstractValidationBean<TestEntity> {

        protected TestValidator(MessageTranslator messageTranslator) {
            super(messageTranslator);
        }

        @Override
        protected void validateObject(TestEntity entity) {
            if (entity.getCode() == null) {
                addMessage(ERROR_MESSAGE_1);
            }
        }

        @Override
        protected boolean isIdentityValid(TestEntity entity) {
            if (entity == null) {
                addMessage(ERROR_MESSAGE_1);
            } else {
                if (entity.getId() == null) {
                    addMessage(ERROR_MESSAGE_1);
                }
            }
            return super.isIdentityValid(entity);
        }

        @Override
        public Class<?> type() {
            return TestValidator.class;
        }
    }
}