package com.datasoft.cappitalpi.core.config.resource;

import com.datasoft.cappitalpi.core.TestUtils;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MessageTranslatorTest {

    private MessageTranslator messageTranslator;

    @BeforeEach
    void setup() {
        messageTranslator = TestUtils.messageTranslator();
    }

    @Test
    void givenNullMessage_thenTranslate_thenReturnEmpty() {
        String message = messageTranslator.getMessage(null, (String) null);
        Truth.assertThat(message).isEmpty();
    }

    @Test
    void givenNullParams_thenTranslate_thenReturnMessage() {
        String message = messageTranslator.getMessage(CoreMessageIdResource.Validation.BAD_REQUEST, (String) null);
        Truth.assertThat(message.length()).isGreaterThan(0);
    }

    @Test
    void givenNoParams_thenTranslate_thenReturnMessage() {
        String message = messageTranslator.getMessage(CoreMessageIdResource.Validation.DATA_INTEGRITY_VIOLATION);
        Truth.assertThat(message.length()).isGreaterThan(0);
    }

    @Test
    void givenValidParameter_thenTranslate_thenReturnMessage() {
        String message = messageTranslator.getMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "name");
        Truth.assertThat(message.length()).isGreaterThan(0);
    }
}