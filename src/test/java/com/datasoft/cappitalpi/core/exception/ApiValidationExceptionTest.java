package com.datasoft.cappitalpi.core.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class ApiValidationExceptionTest {

    @Test
    public void givenMessage_whenApiValidationException_thenReturnMessage() {
        String message = "exception message";
        ApiValidationException exception = new ApiValidationException(message);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenMessage_whenApiValidationException_thenReturnMessageAndCause() {
        String message = "exception message";
        RuntimeException cause = new RuntimeException("any cause");
        ApiValidationException exception = new ApiValidationException(message, cause);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
        Truth.assertThat(exception.getCause()).isEqualTo(cause);
    }
}