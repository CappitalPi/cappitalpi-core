package com.datasoft.cappitalpi.core.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class BadRequestExceptionTest {

    @Test
    public void givenMessage_whenBadRequestException_thenReturnMessage() {
        String message = "exception message";
        BadRequestException exception = new BadRequestException(message);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenMessage_whenBadRequestException_thenReturnMessageAndCause() {
        String message = "exception message";
        RuntimeException cause = new RuntimeException("any cause");
        BadRequestException exception = new BadRequestException(message, cause);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
        Truth.assertThat(exception.getCause()).isEqualTo(cause);
    }
}