package com.datasoft.cappitalpi.core.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class ForbiddenExceptionTest {

    @Test
    public void givenMessage_whenForbiddenException_thenReturnMessage() {
        String message = "exception message";
        ForbiddenException exception = new ForbiddenException(message);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenMessage_whenForbiddenException_thenReturnMessageAndCause() {
        String message = "exception message";
        RuntimeException cause = new RuntimeException("any cause");
        ForbiddenException exception = new ForbiddenException(message, cause);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
        Truth.assertThat(exception.getCause()).isEqualTo(cause);
    }

}