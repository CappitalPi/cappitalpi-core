package com.datasoft.cappitalpi.core.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class ValidationExceptionTest {

    @Test
    public void givenMessage_whenValidationException_thenReturnMessage() {
        String message = "exception message";
        ValidationException exception = new ValidationException(message);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenMessage_whenValidationException_thenReturnCause() {
        RuntimeException cause = new RuntimeException("any cause");
        ValidationException exception = new ValidationException(cause);
        Truth.assertThat(exception.getCause()).isEqualTo(cause);
    }

    @Test
    public void givenMessage_whenValidationException_thenReturnMessageAndCause() {
        String message = "exception message";
        RuntimeException cause = new RuntimeException("any cause");
        ValidationException exception = new ValidationException(message, cause, false, false);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
        Truth.assertThat(exception.getCause()).isEqualTo(cause);
    }

}