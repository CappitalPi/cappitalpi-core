package com.datasoft.cappitalpi.core.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class AppExceptionTest {

    @Test
    public void givenMessage_whenAppException_thenReturnMessage() {
        String message = "exception message";
        AppException exception = new AppException(message);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenMessage_whenAppException_thenReturnMessageAndCause() {
        String message = "exception message";
        RuntimeException cause = new RuntimeException("any cause");
        AppException exception = new AppException(message, cause);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
        Truth.assertThat(exception.getCause()).isEqualTo(cause);
    }
}