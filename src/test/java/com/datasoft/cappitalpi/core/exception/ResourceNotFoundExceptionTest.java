package com.datasoft.cappitalpi.core.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class ResourceNotFoundExceptionTest {

    @Test
    public void givenMessage_whenAppException_thenReturnMessage() {
        final String resourceName = "resource name";
        final String fieldName = "field name";
        final String fieldValue = "field value";

        ResourceNotFoundException exception = new ResourceNotFoundException(resourceName, fieldName, fieldValue);
        Truth.assertThat(exception.getResourceName()).isEqualTo(resourceName);
        Truth.assertThat(exception.getFieldName()).isEqualTo(fieldName);
        Truth.assertThat(exception.getFieldValue()).isEqualTo(fieldValue);
        Truth.assertThat(exception.getMessage()).isEqualTo(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue));
    }

}