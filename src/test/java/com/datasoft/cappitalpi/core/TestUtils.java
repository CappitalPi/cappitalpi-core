package com.datasoft.cappitalpi.core;

import com.datasoft.cappitalpi.core.config.CoreConfig;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import org.springframework.context.MessageSource;

public final class TestUtils {

    private TestUtils() {

    }

    public static MessageSource messageSource() {
        return new CoreConfig().coreMessageSource();
    }


    public static String formatMessage(String messageId, String... params) {
        return messageTranslator().getMessage(messageId, params);
    }

    public static MessageTranslator messageTranslator() {
        return new MessageTranslator(messageSource());
    }
}
