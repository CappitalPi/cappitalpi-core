package com.datasoft.cappitalpi.core;

import jakarta.persistence.Column;

public class ChildOfTestEntity extends TestEntity {

    @Column(length = 30, nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}