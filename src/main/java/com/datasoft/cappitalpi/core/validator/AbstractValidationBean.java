package com.datasoft.cappitalpi.core.validator;

import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportContext;
import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportType;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.util.StringConstantUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractValidationBean<T> implements ValidationBean<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractValidationBean.class);

    private StringBuffer errors;
    private AnnotationSupportContext context;
    private boolean skipValidation;
    private final MessageTranslator messageTranslator;

    protected AbstractValidationBean(MessageTranslator messageTranslator) {
        this.messageTranslator = messageTranslator;
    }

    @Autowired
    public void setContext(AnnotationSupportContext context) {
        this.context = context;
    }

    protected void clearErrorMessages() {
        errors = new StringBuffer();
    }

    @Override
    public void validate(T t) throws ValidationException {
        clearErrorMessages();
        if (isIdentityValid(t)) {
            if (!skipValidation) {
                validateAnnotations(t);
            }
            if (!hasErrors()) {
                validateObject(t);
            }
        }
        skipValidation = false;
        if (hasErrors()) {
            throw new ValidationException(this.getErrorMessages());
        }
    }

    protected void validateAnnotations(T t) {
        if (context == null) {
            LOGGER.warn("Abstract Validation is null for {}", this.getClass().getName());
        } else {
            for (AnnotationSupportType type : AnnotationSupportType.values()) {
                String message = context.validate(type, t);
                if (StringUtils.isNotBlank(message)) {
                    addMessage(message);
                }
            }
        }
    }

    protected abstract void validateObject(T t) throws ValidationException;

    protected boolean isIdentityValid(T t) {
        return !hasErrors();
    }

    protected void addMessage(String message) {
        if (errors == null) {
            errors = new StringBuffer();
        }
        errors.append(message);
        errors.append(StringConstantUtils.R_BREAK_LINE);
        errors.append(StringConstantUtils.N_BREAK_LINE);
    }

    public String formatMessage(String messageId, String... params) {
        return messageTranslator.getMessage(messageId, params);
    }

    protected boolean hasErrors() {
        return errors != null && !errors.toString().isEmpty();
    }

    protected String getErrorMessages() {
        if (errors != null) {
            return StringUtils.chomp(StringUtils.chomp(errors.toString()));
        }
        return StringUtils.EMPTY;
    }

    public void skipValidations() {
        this.skipValidation = true;
    }
}
