package com.datasoft.cappitalpi.core.validator;

import com.datasoft.cappitalpi.core.exception.ValidationException;

public interface ValidationBean<T> {

    void validate(T t) throws ValidationException;

    Class<?> type();

}
