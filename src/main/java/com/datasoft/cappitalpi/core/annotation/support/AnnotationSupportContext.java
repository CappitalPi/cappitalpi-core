package com.datasoft.cappitalpi.core.annotation.support;

import com.datasoft.cappitalpi.core.exception.AppException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class AnnotationSupportContext {

    private final Map<AnnotationSupportType, AnnotationSupport> strategies;

    public AnnotationSupportContext(Set<AnnotationSupport> strategies) {
        this.strategies = new HashMap<>();
        strategies.forEach(strategy -> this.strategies.put(strategy.type(), strategy));
    }

    public String validate(AnnotationSupportType annotationClass, Object entity) {
        final AnnotationSupport strategy = this.strategies.get(annotationClass);
        if (strategy == null) {
            throw new AppException("Invalid Annotation Configuration.");
        }
        return strategy.validate(entity);
    }

}
