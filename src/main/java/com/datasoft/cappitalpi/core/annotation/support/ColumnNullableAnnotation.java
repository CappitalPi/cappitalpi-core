package com.datasoft.cappitalpi.core.annotation.support;

import com.datasoft.cappitalpi.core.config.resource.CoreMessageIdResource;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.core.util.StringConstantUtils;
import jakarta.persistence.Column;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

@Component
public class ColumnNullableAnnotation implements AnnotationSupport {

    private final MessageTranslator messageTranslator;

    public ColumnNullableAnnotation(MessageTranslator messageTranslator) {
        this.messageTranslator = messageTranslator;
    }

    @Override
    public String validate(Object t) {
        StringBuilder message = new StringBuilder();
        if (t != null) {
            try {
                Class<?> clazz = t.getClass();
                while (!clazz.equals(Object.class)) {
                    validateFields(t, message, clazz.getDeclaredFields());
                    clazz = clazz.getSuperclass();
                }
            } catch (IllegalAccessException e) {
                throw new AppException("Could not validate field lengths : " + e.getMessage());
            }
        }
        if (StringUtils.isNotBlank(message.toString())) {
            return StringUtils.chomp(StringUtils.chomp(message.toString()));
        }
        return null;
    }

    private void validateFields(Object t, StringBuilder message, Field[] fields) throws IllegalAccessException {
        for (Field field : fields) {
            field.setAccessible(true);
            Column column = field.getAnnotation(Column.class);
            if (column != null && !column.nullable()) {
                if (isFieldEmpty(field, t)) {
                    message.append(messageTranslator.getMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, field.getName()));
                    message.append(StringConstantUtils.R_BREAK_LINE).append(StringConstantUtils.N_BREAK_LINE);
                }
            }
            field.setAccessible(false);
        }
    }

    private boolean isFieldEmpty(Field field, Object t) throws IllegalAccessException {
        Object value = field.get(t);
        if (value == null) {
            return true;
        }
        if (String.class.getTypeName().equals(field.getType().getTypeName())) {
            String valueStr = (String) value;
            return StringUtils.isBlank(StringUtils.trimToEmpty(valueStr));
        }
        return false;
    }

    @Override
    public AnnotationSupportType type() {
        return AnnotationSupportType.COLUMN_NULLABLE;
    }
}
