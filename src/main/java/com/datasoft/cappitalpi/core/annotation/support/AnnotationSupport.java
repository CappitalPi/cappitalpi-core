package com.datasoft.cappitalpi.core.annotation.support;

public interface AnnotationSupport {

    String validate(Object t);

    AnnotationSupportType type();
}
