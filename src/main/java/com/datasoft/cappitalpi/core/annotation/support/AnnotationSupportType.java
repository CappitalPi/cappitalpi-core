package com.datasoft.cappitalpi.core.annotation.support;

public enum AnnotationSupportType {

    LENGTH,
    COLUMN_NULLABLE,
    JOIN_COLUMN_NULLABLE
}
