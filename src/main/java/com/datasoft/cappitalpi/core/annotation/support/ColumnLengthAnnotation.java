package com.datasoft.cappitalpi.core.annotation.support;

import com.datasoft.cappitalpi.core.config.resource.CoreMessageIdResource;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.core.util.StringConstantUtils;
import jakarta.persistence.Column;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

@Component
public class ColumnLengthAnnotation implements AnnotationSupport {

    private final MessageTranslator messageTranslator;

    public ColumnLengthAnnotation(MessageTranslator messageTranslator) {
        this.messageTranslator = messageTranslator;
    }

    @Override
    public String validate(Object t) {
        StringBuilder message = new StringBuilder();
        if (t != null) {
            try {
                Field[] fields = t.getClass().getDeclaredFields();
                for (Field field : fields) {
                    if (String.class.getTypeName().equals(field.getType().getTypeName())) {
                        field.setAccessible(true);
                        Column column = field.getAnnotation(Column.class);
                        if (column != null) {
                            Object value = field.get(t);
                            String valueStr = (String) value;
                            if (valueStr != null && valueStr.length() > column.length()) {
                                message.append(messageTranslator.getMessage(
                                                CoreMessageIdResource.Validation.CANNOT_BE_GREATER_THAN, field.getName(), String.valueOf(column.length())
                                        )
                                );
                                message.append(StringConstantUtils.R_BREAK_LINE).append(StringConstantUtils.N_BREAK_LINE);
                            }
                        }
                        field.setAccessible(false);
                    }
                }
            } catch (IllegalAccessException e) {
                throw new AppException("Could not validate field lengths : " + e.getMessage());
            }
        }
        if (StringUtils.isNotBlank(message.toString())) {
            return StringUtils.chomp(StringUtils.chomp(message.toString()));
        }
        return null;
    }

    @Override
    public AnnotationSupportType type() {
        return AnnotationSupportType.LENGTH;
    }
}
