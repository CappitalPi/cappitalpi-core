package com.datasoft.cappitalpi.core.util;

public final class HeaderConstant {

    public static final String OWNER_UID = "Enxide";
    public static final String REGISTER_UID = "Rexide";

    private HeaderConstant() {
    }
}