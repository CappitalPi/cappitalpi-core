package com.datasoft.cappitalpi.core.util;

public final class StringConstantUtils {

    public static final String EMPTY = "";
    public static final String DASH = "-";
    public static final String SPACE = " ";
    public static final String TWO_POINTS = ":";
    public static final String R_BREAK_LINE = "\r";
    public static final String N_BREAK_LINE = "\n";
    public static final String SLASH = "/";
    public static final String BIG_DASH = "—";
    public static final String BR = "<br/>";
    public static final String COMMA = ",";

    private StringConstantUtils() {
    }
}
