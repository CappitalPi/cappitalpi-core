package com.datasoft.cappitalpi.core.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public final class UidUtils {

    private static final String KEY = "CAPPI7-9328-9782";
    private static final Logger LOGGER = LoggerFactory.getLogger(UidUtils.class);

    private UidUtils() {
    }

    public static Long getDecId(String encid) {
        if (StringUtils.isBlank(encid)) {
            return null;
        }
        try {
            byte[] message = Base64.getUrlDecoder().decode(encid.getBytes());
            Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
            if (cipher != null) {
                byte[] dMessage = cipher.doFinal(message);
                String d = new String(dMessage);
                return Long.valueOf(d);
            }
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            LOGGER.error("Could not get the decid. {}", e.getMessage(), e);
        }
        LOGGER.warn("Could not get the decid");
        return null;
    }

    public static String getEncId(Long id) {
        if (id == null) {
            return null;
        }
        try {
            String message = id.toString();
            Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
            if (cipher != null) {
                byte[] encryptedMessageBytes = cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));
                return Base64.getUrlEncoder().encodeToString(encryptedMessageBytes);
            }
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            LOGGER.error("Error encrypting encid {}", e.getMessage(), e);
        }
        LOGGER.warn("Could not get the encid");
        return null;
    }

    private static Cipher getCipher(int encryptMode) {
        try {
            byte[] encryptionKeyBytes = KEY.getBytes();
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKey secretKey = new SecretKeySpec(encryptionKeyBytes, "AES");
            cipher.init(encryptMode, secretKey);
            return cipher;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            LOGGER.error("Error getting encrypt object. {}", e.getMessage(), e);
            return null;
        }
    }

}
