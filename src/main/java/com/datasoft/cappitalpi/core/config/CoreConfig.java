package com.datasoft.cappitalpi.core.config;

import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupport;
import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportContext;
import com.datasoft.cappitalpi.core.annotation.support.ColumnLengthAnnotation;
import com.datasoft.cappitalpi.core.annotation.support.ColumnNullableAnnotation;
import com.datasoft.cappitalpi.core.annotation.support.JoinColumnNullableAnnotation;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.repository.RepositoryUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.repository.support.Repositories;
import org.springframework.web.context.WebApplicationContext;

import java.util.Set;

@Configuration
public class CoreConfig {

    public static final String[] BASE_NAMES = new String[]{
            "classpath:messages/core-messages"
    };

    @Bean
    public MessageSource coreMessageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(BASE_NAMES);
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setDefaultLocale(I18NConfig.DEFAULT_LOCALE);
        return messageSource;
    }

    @Bean
    public MessageTranslator messageTranslator() {
        return new MessageTranslator(coreMessageSource());
    }

    @Bean
    public ColumnLengthAnnotation columnLengthAnnotation() {
        return new ColumnLengthAnnotation(messageTranslator());
    }

    @Bean
    public ColumnNullableAnnotation columnNullableAnnotation() {
        return new ColumnNullableAnnotation(messageTranslator());
    }

    @Bean
    public JoinColumnNullableAnnotation joinColumnNullableAnnotation() {
        return new JoinColumnNullableAnnotation(messageTranslator());
    }

    @Bean
    public AnnotationSupportContext annotationSupportContext(Set<AnnotationSupport> strategies) {
        return new AnnotationSupportContext(strategies);
    }

    @Bean
    public RepositoryUtils repositoryUtils(WebApplicationContext appContext) {
        return new RepositoryUtils(new Repositories(appContext));
    }

}
