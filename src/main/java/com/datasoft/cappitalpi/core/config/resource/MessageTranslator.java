package com.datasoft.cappitalpi.core.config.resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class MessageTranslator {

    private final MessageSource messageSource;

    public MessageTranslator(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessage(String messageId, String... params) {
        if (StringUtils.isBlank(messageId)) {
            return StringUtils.EMPTY;
        }
        Locale locale = LocaleContextHolder.getLocale();
        return this.messageSource.getMessage(messageId, params, locale);
    }
}
