package com.datasoft.cappitalpi.core.config.resource;

public final class CoreMessageIdResource {

    public static final class Validation {
        public static final String MANDATORY_FIELD = "message.validation.mandatory-field";
        public static final String CANNOT_BE_GREATER_THAN = "message.validation.cannot-be-greater-than-field";
        public static final String DATA_INTEGRITY_VIOLATION = "message.validation.data-integrity-violation";
        public static final String SERVER_ERROR = "message.validation.server-error";
        public static final String BAD_REQUEST = "message.validation.bad-request";
    }
}
