package com.datasoft.cappitalpi.core.repository;

import com.datasoft.cappitalpi.core.exception.AppException;
import org.springframework.data.domain.Example;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;

@Component
public class RepositoryUtils {

    private final Repositories repositories;

    public RepositoryUtils(Repositories repositories) {
        this.repositories = repositories;
    }

    @SuppressWarnings("unchecked")
    private <T> GenericRepository<T> getRepository(T entity) {
        Optional<Object> repositoryFor = repositories.getRepositoryFor(entity.getClass());
        return (GenericRepository<T>) repositoryFor.orElse(null);
    }

    /**
     * select ~0 >> 32;
     * #MYSQL	4.294.967.295
     * #JAVA    2.147.483.647
     */
    public <T> void generateCode(T entity) {
        try {
            Method getCode = entity.getClass().getMethod("getCode");
            Object getValue = getCode.invoke(entity);
            if (getValue == null) {
                Method setCode = entity.getClass().getMethod("setCode", Long.class);
                Long newCode = (long) (Math.random() * ((Integer.MAX_VALUE - 1) + 1)) + 1;
                setCode.invoke(entity, newCode);
                Optional<T> one = getRepository(entity).findOne(Example.of(entity));
                if (one.isPresent()) {
                    setCode.invoke(entity, new Object[]{null});
                    generateCode(entity);
                }
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new AppException(e.getMessage(), e);
        }
    }

    //TODO: Verificar se o código está na própria lista.
    public <T> void generateCodes(List<T> list) {
        if (!CollectionUtils.isEmpty(list)) {
            list.forEach(this::generateCode);
        }
    }

}
