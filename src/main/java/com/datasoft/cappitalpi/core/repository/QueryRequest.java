package com.datasoft.cappitalpi.core.repository;

import java.util.HashMap;
import java.util.Map;

public class QueryRequest {

    private final String query;
    private final Map<String, Object> params;

    public QueryRequest(String query, Map<String, Object> params) {
        this.query = query;
        this.params = params;
    }

    public String getQuery() {
        return query;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public static class Builder {

        private String query;
        private Map<String, Object> params;

        public Builder withQuery(String query) {
            this.query = query;
            return this;
        }

        public Builder withParams(Map<String, Object> params) {
            this.params = params;
            return this;
        }

        public QueryRequest build() {
            if (params == null) {
                params = new HashMap<>();
            }
            return new QueryRequest(query, params);
        }

    }
}
