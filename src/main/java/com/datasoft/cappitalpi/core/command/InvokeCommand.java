package com.datasoft.cappitalpi.core.command;

import java.util.ArrayList;
import java.util.List;

public class InvokeCommand implements Command {

    private final List<Command> commands;

    private InvokeCommand(List<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        commands.forEach(Command::execute);
    }

    public static class Builder {

        private final List<Command> commands = new ArrayList<>();

        public Builder add(Command command) {
            commands.add(command);
            return this;
        }

        public InvokeCommand build() {
            return new InvokeCommand(commands);
        }
    }

}
