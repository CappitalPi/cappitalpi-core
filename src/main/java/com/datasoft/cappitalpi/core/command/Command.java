package com.datasoft.cappitalpi.core.command;

public interface Command {

    void execute();
}
