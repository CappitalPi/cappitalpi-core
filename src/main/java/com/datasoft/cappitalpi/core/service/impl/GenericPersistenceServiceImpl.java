package com.datasoft.cappitalpi.core.service.impl;

import com.datasoft.cappitalpi.core.observer.BaseObservable;
import com.datasoft.cappitalpi.core.observer.BaseObserver;
import com.datasoft.cappitalpi.core.repository.GenericRepository;
import com.datasoft.cappitalpi.core.service.GenericPersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class GenericPersistenceServiceImpl<E> implements GenericPersistenceService<E> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericPersistenceServiceImpl.class);

    protected abstract GenericRepository<E> getRepository();

    private final BaseObservable observable = new BaseObservable();

    @Override
    @Transactional
    public E persist(E e, BaseObserver... observers) {
        final E data = getRepository().save(e);
        notifyEvent(e, observers);
        return data;
    }

    @Override
    @Transactional
    public E update(E e, Long id, BaseObserver... observers) {
        if (id != null && id > 0) {
            final E data = getRepository().save(e);
            notifyEvent(e, observers);
            return data;
        }
        LOGGER.error("Could not update object {}. ID not found", e.getClass().getName());
        return null;
    }

    @Override
    @Transactional
    public E remove(Long id, BaseObserver... observers) {
        E e = findById(id);
        if (e != null) {
            getRepository().delete(e);
            notifyEvent(e, observers);
        } else {
            LOGGER.error("Could not remove object (id = {})", id);
        }
        return e;
    }

    @Override
    public List<E> findAll() {
        return getRepository().findAll();
    }

    @Override
    public E findById(Long id) {
        return getRepository().findById(id).orElse(null);
    }

    protected void notifyEvent(E data, BaseObserver... observers) {
        if (observers != null && observers.length > 0) {
            int index = 0;
            for (BaseObserver o : observers) {
                if (o != null) {
                    observable.add(index, o);
                    index++;
                }
            }
        }
        this.observable.notifyObservers(data);
    }

}
