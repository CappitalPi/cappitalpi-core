package com.datasoft.cappitalpi.core.service;

import com.datasoft.cappitalpi.core.observer.BaseObserver;

import java.util.List;

public interface GenericPersistenceService<T> {

    T persist(T t, BaseObserver... observers);

    T update(T t, Long id, BaseObserver... observers);

    T remove(Long id, BaseObserver... observers);

    List<T> findAll();

    T findById(Long id);

}
