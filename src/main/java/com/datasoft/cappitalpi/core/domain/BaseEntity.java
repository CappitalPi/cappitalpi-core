package com.datasoft.cappitalpi.core.domain;

import com.datasoft.cappitalpi.core.util.UidUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;

@MappedSuperclass
public class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code", updatable = false, nullable = false)
    private Long code;

    @Transient
    private String uid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getUid() {
        if (id != null) {
            uid = UidUtils.getEncId(id);
        }
        return uid;
    }

    @JsonIgnore
    public boolean isNew() {
        return (this.id == null);
    }

}

