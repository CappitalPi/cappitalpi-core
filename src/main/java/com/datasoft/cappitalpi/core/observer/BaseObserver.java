package com.datasoft.cappitalpi.core.observer;

public interface BaseObserver {

    void notifyObserver(Object arg);

}