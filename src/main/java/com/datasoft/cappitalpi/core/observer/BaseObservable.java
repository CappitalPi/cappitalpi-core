package com.datasoft.cappitalpi.core.observer;

import java.util.Map;
import java.util.TreeMap;

public class BaseObservable {

    private final Map<Integer, BaseObserver> observables = new TreeMap<>();

    public void add(Integer index, BaseObserver observable) {
        this.observables.put(index, observable);
    }

    public void notifyObservers(Object arg) {
        try {
            for (Map.Entry<Integer, BaseObserver> entryMap : this.observables.entrySet()) {
                if (entryMap.getValue() != null) {
                    entryMap.getValue().notifyObserver(arg);
                }
            }
        } finally {
            observables.clear();
        }
    }

}