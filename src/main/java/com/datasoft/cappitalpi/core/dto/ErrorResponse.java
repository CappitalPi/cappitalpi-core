package com.datasoft.cappitalpi.core.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ErrorResponse {

    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private String date;
    private int status;
    private String message;
    private String details;
    private boolean isBusinessRule;

    public ErrorResponse() {
    }

    public ErrorResponse(int status, String message, String details) {
        this.date = new SimpleDateFormat(DATE_TIME_FORMAT).format(new Date());
        this.status = status;
        this.message = message;
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

    public boolean isBusinessRule() {
        return isBusinessRule;
    }

    public void setBusinessRule(boolean businessRule) {
        isBusinessRule = businessRule;
    }
}