package com.datasoft.cappitalpi.core.dto;

import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageResultDto<T> {

    private int currentPage;
    private int pageSize;
    private long totalElements;
    private List<T> content;
    private Map<String, BigDecimal> balances;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPages() {
        if (!CollectionUtils.isEmpty(content) && pageSize > 0 && totalElements > 0) {
            return (int) Math.ceil((double) totalElements / pageSize);
        }
        return 0;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public Map<String, BigDecimal> getBalances() {
        return balances;
    }

    public void addBalance(String key, BigDecimal amount) {
        if (this.balances == null) {
            this.balances = new HashMap<>();
        }
        this.balances.put(key, amount);
    }
}
